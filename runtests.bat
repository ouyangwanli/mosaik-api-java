@echo off
IF "%~1" equ "" (
    echo You need to pass the path to your Python 3 executable as first parameter
    EXIT /B 1
) ELSE (
    ant
    virtualenv -p %1 .\test_env
    @call .\test_env\Scripts\activate.bat
    pip install -U pytest simpy.io mosaik
    py.test %2 tests\
    @call .\test_env\Scripts\deactivate.bat
    rmdir /Q /S test_env
)